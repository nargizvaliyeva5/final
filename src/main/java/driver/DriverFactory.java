package driver;

import exceptions.DriverNotSupportedException;
import impl.ChromeWebDriver;
import impl.EdgeWebDriver;
import impl.FirefoxWebDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.Locale;

public class DriverFactory {
    private String browserType = System.getProperty("browser").toLowerCase(Locale.ROOT);


    public WebDriver getDriver() throws DriverNotSupportedException {
        switch (this.browserType) {
            case "chrome": {
                return new EventFiringWebDriver(new ChromeWebDriver().newDriver());
            }
            case "firefox": {
                return new EventFiringWebDriver(new FirefoxWebDriver().newDriver());
            }
            case "edge": {
                return new EventFiringWebDriver(new EdgeWebDriver().newDriver());
            }
            default:
                throw new DriverNotSupportedException(this.browserType);
        }

    }
}

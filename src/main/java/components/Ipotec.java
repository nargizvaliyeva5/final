package components;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.DaxiliIpotec;
import pages.IpotecPage;

public class Ipotec extends AbsBaseComponent{
    public Ipotec (WebDriver driver){
        super(driver);}

    @FindBy(css="li>span>a[href='https://abb-bank.az/az/ferdi/kreditler/ipoteka-kreditleri']")
    private WebElement Ipotec;
    public IpotecPage Ipotecbutton(){
       Ipotec.click();
       return new IpotecPage(driver);

    }
@FindBy(css="span>a[href='https://abb-bank.az/az/ferdi/kreditler/daxili-ipoteka-krediti#applyForm']")
    private WebElement DaxiliIpotec;
    public IpotecPage ApplyDaxiliIpotec(){
       DaxiliIpotec.click();
        return new IpotecPage(driver);


    }




    }



package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class Mastercard extends AbsBasePage {

        public Mastercard (WebDriver driver) {
            super(driver);
}

      @FindBy(css="img[alt='TamKart MasterCard Standard PayPass - Debet']")
      private WebElement TamMaster;
       @FindBy (css="h1")
       private WebElement h1;
      String h1text= h1.getText();
      public Mastercard check() {
          Assert.assertEquals(h1text, "TamKart MasterCard Debet");


          return new Mastercard(driver);

      }}

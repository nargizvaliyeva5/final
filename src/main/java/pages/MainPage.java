package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class MainPage extends AbsBasePage{

public MainPage(WebDriver driver){super(driver);}
    public void mainpagetitleshouldbesameas() {
        String title = driver.getTitle();
        Assert.assertEquals(title, "ABB - Müasir, Faydalı, Universal");
    }
    @FindBy(css="li>span>a[href=\"https://abb-bank.az/az/ferdi/bank-24-7\"]")
    private WebElement Bank7;
    public BankSeven clickbank7() {
        Bank7.click();
        return new BankSeven(driver);
    }
    }

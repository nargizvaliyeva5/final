package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.Iterator;
import java.util.Set;


public class MobilBank extends AbsBasePage {

        public MobilBank(WebDriver driver) {
            super(driver);}
     @FindBy(css="\".fw-600.fs-26.fs-lg-44.lh-34.lh-lg-62.mt-3.mt-lg-0.mb-4.pt-lg-0\"")
     private WebElement header;
     public MobilBank newwindow(){



    String mainWindow = driver.getWindowHandle();
    Set<String> windowsId = driver.getWindowHandles();
    Iterator<String> iterator = windowsId.iterator();
        while (iterator.hasNext()) {
        String childWindow = iterator.next();
        if (!mainWindow.equalsIgnoreCase(childWindow)) {
            driver.switchTo().window(childWindow);
            WebElement header = driver.findElement(By.cssSelector(".fw-600.fs-26.fs-lg-44.lh-34.lh-lg-62.mt-3.mt-lg-0.mb-4.pt-lg-0"));
            Assert.assertEquals(header.getText(), "ABB mobile – Sadə və sürətli");
        }}
         return null;
    }}

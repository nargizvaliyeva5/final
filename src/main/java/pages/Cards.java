package pages;

import data.CardData;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;


public class Cards extends AbsBasePage {
    JavascriptExecutor js;

    public Cards(WebDriver driver) {
        super(driver);
        js = (JavascriptExecutor) driver;

    }
    @FindBy(css = "#js-header-s3>.pr-4")
    private List<WebElement> webElementList;

    private WebElement kartlar = webElementList.get(3);
    public void cardspagestitleshouldbesameas(){
        String title= driver.getTitle();
        Assert.assertEquals(title, "Online Kart Sifarişi - Debet və Kredit kart- ABB Bank Kartları");

    }
    @FindBy (css="div>a>div>h3")
    private List<WebElement> listOfCards;
//    @FindBy(css ="div.p-2>a[href='https://abb-bank.az/az/ferdi/kartlar/tamkart-debet-kartlari/tamkart-mastercard-standard-paypass-debet']")
//    private WebElement Mastercard;
    public Cards choosecard(CardData data){

       for (WebElement card: listOfCards) {
            if (card.getText().equalsIgnoreCase(data.getName())) {
                Assert.assertEquals(data.getName(),card.getText());
                card.click();
                break;
           }
        }


        return new Cards(driver);
    }



    }

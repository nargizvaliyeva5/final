package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class BankSeven extends AbsBasePage {

    public BankSeven(WebDriver driver) {
        super(driver);}
    @FindBy(css="li>span>a[href=\"https://abb-bank.az/az/ferdi/bank-24-7\"]")
    private WebElement Bank7;
    public BankSeven clickbank7() {
        Bank7.click();
        return new BankSeven(driver);
    }
    @FindBy(css="#js-hover-link > span:nth-child(1)")
    private WebElement MobilBanking;
    public BankSeven clickMobileBank(){
       MobilBanking.click();
       return new BankSeven(driver);

    }
}

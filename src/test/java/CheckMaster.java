import data.CardData;
import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.BankSeven;
import pages.Cards;
import pages.MainPage;
import pages.Mastercard;

public class CheckMaster {
    private WebDriver driver;

    @BeforeMethod
    public void init () throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
    }
    @Test
    public void CheckMaster() {
        new MainPage(driver)
                .open("");
        new Cards(driver)
                .open("/az/ferdi/kartlar");
        new Cards(driver)
                .choosecard(CardData.Kart2);
        new Mastercard(driver)
                .open("/az/ferdi/kartlar/tamkart-debet-kartlari/tamkart-mastercard-standard-paypass-debet");
        new Mastercard(driver)
                .check();




    }
    @AfterMethod
    public void close () {
        if (this.driver != null) {
            this.driver.close();
            this.driver.quit();
        }
    }
}

import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.BankSeven;
//import pages.Cards;
import pages.MainPage;
import pages.MobilBank;

import static data.CardData.Kart1;

public class TestBankSeven {
    private WebDriver driver;

    @BeforeMethod
    public void init () throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
    }

     @Test
     public void headerofmobilebank(){
        new MainPage(driver)
                .open("");
        new MainPage(driver)
                .clickbank7();
        new BankSeven(driver)
                .open("/az/ferdi/bank-24-7");
        new BankSeven(driver)
                .clickMobileBank();
        new MobilBank(driver)
                .open("/az/ferdi/bank-24-7/abb-mobile");
        new MobilBank(driver)
                .newwindow();
     }


    @AfterMethod
    public void close () {
        if (this.driver != null) {
            this.driver.close();
            this.driver.quit();
        }
    }}



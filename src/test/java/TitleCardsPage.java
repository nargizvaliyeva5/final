import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.Cards;
import pages.MainPage;

import static data.CardData.Kart1;

public class TitleCardsPage {

        private WebDriver driver;

        @BeforeMethod
        public void init () throws DriverNotSupportedException {
            this.driver = new DriverFactory().getDriver();
        }
        @Test
        public void titleofmainpage() {


            new MainPage(driver)
                    .open("");
            new MainPage(driver)
                    .clickbank7();
            new Cards(driver)
                    .open("/az/ferdi/kartlar");
            new Cards(driver)
                    .cardspagestitleshouldbesameas();
            new Cards(driver)
                    .choosecard(Kart1);






        }
        @AfterMethod
        public void close () {
            if (this.driver != null) {
                this.driver.close();
                this.driver.quit();
            }
        }}
